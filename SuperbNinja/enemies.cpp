//------------------------------------------------------------------------------------------------------------------------------------//
/// Brief :		This is the control for the enemy characters that the player is in control of.
///				This is used to set attributes and add movement to the game.
//---------------------------------------------------------------------------------------------------------------------------------//
#include "Classes.h"

// Set the class to its defaults
Dog::Dog() { transform= 1; dogx = 0; dogy = -3000;}
// Deconstructor 
Dog::~Dog() { }
// Returning Position Data
double Dog::getDogX() 
{ return dogx; }
double Dog::getDogY() 
{ return dogy; }

// Drawing all the images based off the movement data
void Dog::draw(BITMAP *buffer, BITMAP *dog)
{	
	{ masked_blit(dog, buffer, 1030 , 52 , dogx, dogy,60,70); }
}

// Movement of the dogs and triggers to set a new position
void Dog::movement()
{
	dogy += transform;

	if (dogy >= 480)
	{
		generateNewPosition();
	}

}

void Dog::generateNewPosition()
{
	int temp;
	dogy = -3000;
	temp = (( int ) rand() % 500);

	dogx = temp;

	int  temp2;

	temp2 = ((int ) rand() % 7) + 2;

	transform = temp2;
}

// Set the class to its defaults
Boss::Boss() { health = 0; bossx = 320; bossy = 40; bossFirex = -4000; 
					 bossFirey = - 2500; transform= 0.1; FireTransform = 1.5; }
// Deconstructor 
Boss::~Boss() { }

// Returning Position Data
double Boss::getBossX() 
	{ return bossx; }
double Boss::getBossY() 
	{ return bossy; }
double Boss::getFireX() 
	{ return bossFirex; }
double Boss::getFireY() 
	{ return bossFirey; }
int Boss::deductHealth()
	{health++; return health;}

// Class for the movement of the Boss
void Boss :: followPlayer (double ninx)
{
	if (ninx < bossx)
	{ bossx -= transform;}
	if (ninx > bossx)
	{ bossx += transform;}
}

// Drawing all the images based off the movement data
void Boss::draw(BITMAP *buffer, BITMAP *Boss, BITMAP *Fire)
{	
	masked_blit(Boss, buffer, 0 ,0, bossx, bossy,Boss->w,Boss->h); 
	masked_blit( Fire, buffer, 0 ,0, bossFirex, bossFirey, Fire->w, Fire->h);
}

// Attack the player periodically
void Boss::attack()
{
	bossFirey += FireTransform;
	if ( bossFirey >= 800)
	{ bossFirey = -200;
		bossFirex = bossx;}
}

