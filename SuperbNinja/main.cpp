/// Author:	James Robertson
/// Date:		08/03/2014
/// Game:	"The Ninja"
/// Version	1.0
//---------------------------------------------------------------------------------//
/// Brief :		This is a sidescrolling run and gun game based off of the original
///				that was on the "Sega Master System".
//---------------------------------------------------------------------------------//
/// Include Directries
#include"Init.h"
#include "Classes.h"

int main ()
{
	//Initlise all of the Allegro and Image position data
	allegroInit();
	// Lock the ticker up
	LOCK_VARIABLE( counter );
	LOCK_FUNCTION( tickUp );
	install_int_ex ( tickUp, BPS_TO_TIMER(90) );
	// Current state of the game
	short int gameState = 0;
	// The score
	short int scoreTotal = 0;
	// Number of enemies on screen
	const short int NO_OF_ENEMIES = 10;
	// Booleon Variables
	bool isDead		= false;
	bool quit				= false;
	bool pause			= false;
	bool shot				= false;
	bool removeStar = true;
	bool dogDead[NO_OF_ENEMIES];

	for (int i = 0; i < NO_OF_ENEMIES; i++)
	{ dogDead[i]		= false; }

	// Create the buffer for all of the graphics to be printed onto
	BITMAP *buffer = create_bitmap(640,480);

	//if the game has just begun then go into the story
	if (gameState == 0) 
		gameState = story(buffer, gameState);

	// If the story has finished then go to the menu screen
	if (gameState == 1) 
		gameState = menu(buffer, gameState);

	//Class initilisation
	Ninja playerOne;
	Dog Dogs[NO_OF_ENEMIES];
	Star stars;
	Boss fireLord;

	//All the Bitmaps need to be initilised in the  main  for easier manipulating 
	BITMAP *grass = load_bitmap("Graphics\\Grass.bmp",NULL);
	BITMAP *ninja = load_bitmap("Graphics\\Ninja.bmp",NULL);
	BITMAP *star	 = load_bitmap("Graphics\\Stars.bmp",NULL);
	BITMAP *dog = load_bitmap("Graphics\\Doggies.bmp",NULL);
	BITMAP *fire	= load_bitmap("Graphics\\Fireball.bmp",NULL);
	BITMAP *boss = load_bitmap("Graphics\\Boss.bmp",NULL);
	BITMAP *esc	 = load_bitmap("Graphics\\ExitGame.bmp",NULL);

	// Font Initilisation
	FONT *score	 = load_font("Graphics\\ScoreFont.pcx",NULL,NULL);

	// Music is loaded into the game 
	SAMPLE *grassyFields = load_sample("Sounds\\Grassy Field.wav"); 
	SAMPLE *death = load_sample("Sounds\\DeathScream.wav"); 
	SAMPLE *dogWhimper = load_sample("Sounds\\DogWhimper.wav");
	SAMPLE *bossHit = load_sample("Sounds\\BossHit.wav");

	// Play at the start of the game
	play_sample(grassyFields, 255, 100, 1000, 1);

	for (int i = 0; i < NO_OF_ENEMIES; i++)
	{ Dogs[i].generateNewPosition(); }

	//Whilst game is not paused or quit , loop to update. Could I put this into a function? 
	while (quit == false)
	{
		while ( counter > 0 )
		{
			//Poll the players input
			if (key[KEY_ESC])
				quit = true;
			if (key[KEY_W])	
				playerOne.movement(2);
			if (key[KEY_A])	
				playerOne.movement(4);
			if (key[KEY_D])	
				playerOne.movement(6);
			if (key[KEY_S])	
				playerOne.movement(8);

			// If shoot is press and it is possible to shoot
			if ((key[KEY_SPACE] && shot  == false))
			{
				shot = true;
				stars.setPosition(playerOne.getNinjaX());
			}
			// Movement for the Dogs
			for (int i = 0; i < NO_OF_ENEMIES; i++) 
			{
				Dogs[i].movement();
			}
			// If the shot has been fired, wait till - 20 to make true
			if (shot == true) 
			{
				if (stars.getStarY() <= - 20)
				{shot = false;}
			}
			// Move Stars
			stars.movement();
			// Decrease Counter
			counter -= 1;
		}

		// Draw images onto buffer
		playerOne.draw(buffer, ninja, grass);
		stars.draw(buffer, star);
		masked_blit(esc, buffer,0,0,0,450,esc->w,esc->h);
		textprintf_ex(buffer, score,20,0,(0,0,0),-1, "Score : %d", scoreTotal);
		// Spawn the boss in 
		if (playerOne.getGrassY() > -20)
		{
			fireLord.draw(buffer,boss,fire);
			fireLord.followPlayer(playerOne.getNinjaX());
			fireLord.attack();
			// Collision ( Stars + Boss )
			if	(((stars.getStarY() + 32 > fireLord.getBossY()) &&
				(stars.getStarX() + 32 > fireLord.getBossX()+20) &&
				(stars.getStarY() < fireLord.getBossY()+20) &&				
				(stars.getStarX() < fireLord.getBossX()+20)))
			{
				scoreTotal += 100;
				play_sample(bossHit,10,255,1000,0);
				rest(10);
				fireLord.deductHealth();

				// if he is over 100 - you win!
				if (fireLord.deductHealth() > 100)
				{ 
					rest ( 1000 ) ;
					youWin(buffer,score,scoreTotal);
					quit = true;
				}
			}
		}
		//int dir = Dogs[0].aiMove (playerOne.getNinjaX())
		for (int i = 0; i < NO_OF_ENEMIES; i++) 
		{
			// Draw the dogs onto the buffer
			Dogs[i].draw(buffer, dog);
			// Collision ( Stars + Dogs )
			if (((stars.getStarY() + 20 > Dogs[i].getDogY()) &&
				(stars.getStarX() + 32 > Dogs[i].getDogX()+32) &&
				(stars.getStarY() < Dogs[i].getDogY()+20) &&		
				(stars.getStarX() < Dogs[i].getDogX()+32)))
			{
				dogDead[i] = true;
			}	

			if (dogDead[i] == true) 
			{
				scoreTotal += 50;
				play_sample(dogWhimper, 255, 100, 1000, 0);
				rest(20);
				Dogs[i].generateNewPosition();
				dogDead[i] = false;
			}
			if (quit == false)
			{
				// if you are hit by fire - Dead
				// Collision ( Player + Fire )
				if (((playerOne.getNinjaY() + 32 > fireLord.getFireY()) &&
					(playerOne.getNinjaX() + 32 > fireLord.getFireX()+20) &&
					(playerOne.getNinjaY() < fireLord.getFireY()+20) &&				
					(playerOne.getNinjaX() < fireLord.getFireX()+20)))
				{
					isDead = true;
				}	
				// Collision ( Player + Dogs )
				if (((playerOne.getNinjaY() + 32 > Dogs[i].getDogY()) &&
					(playerOne.getNinjaX() + 32 > Dogs[i].getDogX()+20) &&
					(playerOne.getNinjaY() < Dogs[i].getDogY()+20) &&				
					(playerOne.getNinjaX() < Dogs[i].getDogX()+20)))
				{
					isDead = true;
				}	

				if (isDead == true) 
				{
					destroy_sample(grassyFields);
					play_sample(death, 255, 100, 1000, 0);
					rest(1000);
					gameOver(buffer); 
					quit = true;
				}
			}
		}
		//Draw everything to the screen
		blit( buffer, screen, 0,0,0,0, buffer->w, buffer->h);

		//Clear the buffer to halt ghosting
		clear_bitmap(buffer);	
	}

	//Destroy all of the bitmaps to save memory locations
	destroy_bitmap(buffer);
	destroy_bitmap(grass);
	destroy_bitmap(ninja);
	destroy_bitmap(dog);
	destroy_bitmap(star);
	destroy_bitmap(fire);
	destroy_bitmap(boss);
	destroy_bitmap(esc);

	// Destroy the font to save memory
	destroy_font(score);

	// Destroy all of the audio files to save memory locations
	destroy_sample(death);
	destroy_sample(dogWhimper);
	destroy_sample(bossHit);

	return 0;
}
//Use Allegro notation to complete game
END_OF_MAIN()