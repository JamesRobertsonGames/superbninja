//------------------------------------------------------------------------------------------------------------------------------------//
/// Brief :		This Header file initilises all of the files at the start and runs everything that is not repeated
//---------------------------------------------------------------------------------------------------------------------------------//

#pragma once
#include <allegro.h>

//Function Declarations
void allegroInit ();
int story (BITMAP *buffer, int gameState);
int menu (BITMAP *buffer, int gameState);
void gameOver (BITMAP*buffer);
void youWin (BITMAP *buffer, FONT *score, short int scoreTotal);

// Timer - This is the only global variable that is needed in allegro
volatile long counter = 0;

// Ticks up the counter
void tickUp()
{ counter++; }

//Initilisation of all the Allegro Library
void allegroInit ()
{
	allegro_init();
	install_keyboard();
	install_mouse();
	install_sound(DIGI_AUTODETECT, MIDI_AUTODETECT, NULL);
	install_timer();
	set_color_depth( 16 );
	set_gfx_mode(GFX_AUTODETECT_WINDOWED, 640, 480, 0, 0 );
	// srand to seed for the time
	srand (( unsigned ) time ( NULL )); 
}

// This is the story that takes place at the start of the game
int story(BITMAP *buffer, int gameState)
{
	// Where the "Story" starts
	double story_y = -5.0;
	// Load BITMAP and SAMPLES used in the story
	BITMAP *story = load_bitmap ("Graphics\\Story.bmp",NULL );
	SAMPLE *mainTheme = load_sample ( "Sounds\\Invincible.wav" ); 
	// Play the sample
	play_sample(mainTheme, 255, 100, 1000, 0);

	do
	{
		// Keep scrolling down
		story_y -= 0.5;
		// Blit the images
		blit( story,buffer, 0,0,0,story_y, story->w, story->h );
		blit( buffer, screen, 0,0,0,0, buffer->w, buffer->h );
		// Rest so it doesn't look jaringly quick
		rest(5);
		// if it has reached the end or the player has pressed ESC then finish
		if ( story_y < -1500 || key[KEY_ESC] ) 
			gameState = 1;

	} while ( gameState == 0 );

	// Destroy all of the data that wont be used 
	destroy_bitmap( story );
	destroy_sample( mainTheme );
	// Return the game state to main
	return gameState;
}

// Menu screen in the game
int menu ( BITMAP *buffer, int gameState )
{
	// Load the data used in this function
	BITMAP *staryCursor = load_bitmap ("Graphics\\Stars.bmp",NULL);
	BITMAP *title	 = load_bitmap ("Graphics\\Title.bmp",NULL);
	SAMPLE *mainTheme = load_sample ("Sounds\\Main Theme.wav"); 
	// Play the sample
	play_sample(mainTheme, 255, 100, 1000, 0);
	// Do this while you have not continued
	do
	{
		rest(5);
		// Blit images to screen
		blit ( title,buffer, 0,0,0,0, title->w, title->h );
		masked_blit ( staryCursor, buffer,0,0,mouse_x,mouse_y,
								staryCursor->w, staryCursor->h );
		blit ( buffer, screen, 0,0,0,0, buffer->w, buffer->h );
		// Clear the buffer
		clear_bitmap( buffer );
		// if the left mouse button is pressed. Proceed
		if ( mouse_b & 1 )
			{ gameState = 2; }

	}while ( gameState == 1 );
	//Destroy unecessary data
	destroy_bitmap( title );
	// Load needed data
	BITMAP * helpScreen = load_bitmap( "Graphics\\HelpScreen.bmp" ,NULL );
	// Do this whilst the button has not been pressed on the mouse
	do{
		// Blit the images to the screen
		blit ( helpScreen, buffer, 0,0,0,0,helpScreen->w, helpScreen->h );
		masked_blit ( staryCursor, buffer,0,0,mouse_x,mouse_y,
								staryCursor->w, staryCursor->h );
		blit( buffer, screen, 0,0,0,0, buffer->w, buffer->h );
		// if the mouse is pressed move into the game
		if ( mouse_b & 1 )
			{ gameState = 3; }
	} while( gameState == 2 );

	destroy_sample ( mainTheme );
	destroy_bitmap ( helpScreen );
	destroy_bitmap ( staryCursor );
	// Return the game state to main
	return gameState;
}

// When the player gets a game over 
void gameOver ( BITMAP*buffer )
{
	// Load up necessary images
	BITMAP *deathScreen = load_bitmap("Graphics\\GameOver.bmp",NULL);
	// Whilst the mouse is not being pressed
	while(!mouse_b & 1)
	{
		// Display images
		blit (deathScreen,buffer,0,0,0,0,deathScreen->w,deathScreen->h);
		blit( buffer, screen, 0,0,0,0, buffer->w, buffer->h);
	}
	// Destroy unecessary images
	destroy_bitmap ( deathScreen );
}

// When you beat the boss to win the game
void youWin ( BITMAP *buffer , FONT *score, short int scoreTotal )
{
	// Load the data we need in this function
	BITMAP *winScreen = load_bitmap("Graphics\\WinScreen.bmp",NULL);
	FONT *FinalScore = load_font("Graphics\\FinalScore.pcx",NULL,NULL);
	// While the mouse is not being pressed
	while(!mouse_b & 1)
	{
		// Display everything on screen
	blit ( winScreen, buffer, 0,0,0,0,winScreen->w,winScreen->h );
	textprintf_ex(buffer, FinalScore,320,260,(255,255,255),-1, "%d", scoreTotal);
	blit ( buffer , screen , 0 , 0 , 0 , 0 , buffer->w , buffer->h );
	}
	destroy_font ( FinalScore );
	destroy_bitmap( winScreen );
}

