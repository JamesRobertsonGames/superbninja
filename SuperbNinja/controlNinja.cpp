//MemCopy - compires memory is low usage
//------------------------------------------------------------------------------------------------------------------------------------//
/// Brief :		This is the control for the Ninja character that the player is in control of.
///				This is used to set attributes and add movement to the game.
//---------------------------------------------------------------------------------------------------------------------------------//
#include "Classes.h"

// Set the class to its defaults
Ninja::Ninja() { transform= 1.5; direction = 2; ninx = 320; 
						niny = 370; grassx = 0; grassy = -2500;}
// Deconstructor 
Ninja::~Ninja() { }
// Returning Position Data
double Ninja::getNinjaX() 
	{ return ninx; }
double Ninja::getNinjaY() 
	{ return niny; }
double Ninja::getGrassY() 
	{ return grassy; }
// Direction Data
int Ninja::Direction() 
	{ return direction; }


// Class for the movement of the ninja
void Ninja::movement(int dir)
{
	if (dir == 2)
	{
		if (grassy < 0) 
			{ grassy += transform; }
		direction = 2;
	}
	else if (dir == 4)
	{
		if (ninx > 0) 
			{ ninx -= transform; }
		direction = 4;
	}
	else if (dir == 6)
	{
		if (ninx < 608)
			{ ninx += transform; }
		direction = 6;
	}
	else if (dir == 8)
	{
		if (grassy > -2300) 
			{ grassy -= transform; }
		direction = 8;
	}
}

// Drawing all the images based off the movement data
void Ninja::draw(BITMAP *buffer, BITMAP *ninja, BITMAP *grass)
{	
	blit(grass,buffer,0,0,0,grassy,grass->w,grass->h);
	if (Direction() == 2)	
		{ masked_blit(ninja, buffer, 70 ,315, ninx, niny,50,80); }
	else if (Direction() == 4) 
		{ masked_blit(ninja, buffer, 0,230, ninx, niny,60,80); }
	else if (Direction() == 6) 
		{ masked_blit(ninja, buffer, 70,230, ninx, niny,60,80); }
	else if (Direction() == 8) 
		{ masked_blit(ninja, buffer, 10,0, ninx, niny,50,100); }
}


