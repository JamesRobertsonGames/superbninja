//------------------------------------------------------------------------------------------------------------------------------------//
/// Brief :		This is the control for the Star character that the player is in control of.
///				This is used to set attributes and add movement and animation to the game.
//---------------------------------------------------------------------------------------------------------------------------------//
#include "Classes.h"

// Set the class to its defaults
Star::Star() { transform = 7;
starx = -30; stary = 0;}
// Deconstructor 
Star::~Star() { }
// Returning Position Data
double Star::getStarX() 
{ return starx; }
double Star::getStarY() 
{ return stary; }

// Drawing all the images based off the movement data
void Star::draw(BITMAP *buffer, BITMAP *star)
{	
	masked_blit(star, buffer, 0,0, starx, stary,64,64);
}

void Star::movement()
{
	stary -= transform;
}

void Star::setPosition(double ninx)
{
	stary = 380;
	starx = ninx + 20;
}