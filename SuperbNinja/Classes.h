//------------------------------------------------------------------------------------------------------------------------------------//
/// Brief :		This Header file is just for the creation of the Classes
//---------------------------------------------------------------------------------------------------------------------------------//
#pragma once
#include <allegro.h>

//Class for the player character
class Ninja
{
private:
	// Where the player is facing
	int direction;
	// Ninja & grass positions
	double ninx, niny, grassx, grassy;
	// Speed of the ninja
	float transform;
public:
	// Movement of character
	void movement(int);
	// Draw onto buffer
	void draw(BITMAP *buffer, BITMAP *ninja, BITMAP *grass);
	// Getters
	float Transform() { return transform; }
	double getNinjaX();
	double getNinjaY();
	double getGrassY();
	int Direction();
	// Constructer - Deconstructer
	Ninja();
	~Ninja();
};

// Enemy Dog Class
class Dog
{
private:
	// Position of Dog
	double dogx, dogy;
	// Speed
	float transform;
public:
	// Moving the dog
	void movement();
	// Draw the data onto the buffer
	void draw(BITMAP *buffer, BITMAP *dogs);
	// Randomly generate new position when off screen
	void generateNewPosition();
	// Getters
	float Transform() { return transform; }
	double getDogX();
	double getDogY();
	// Constructor - Deconstructor
	Dog();
	~Dog();
};

// Star Class
class Star
{
private:
	// Position of the ninja star
	double starx, stary;
	// Speed of the ninja star
	float transform;
public:
	// Movement of the ninja star
	void movement();
	// draw the data on screen
	void draw(BITMAP *buffer, BITMAP *star);
	//Setter
	void setPosition(double ninx);
	// Getters
	float Transform() { return transform; }
	double getStarX();
	double getStarY();
	// Constructer - Deconstructer
	Star();
	~Star();
};

class Boss
{
private:
	// Amount of health left
	int health;
	// Boss positions, both attack and enemy
	double bossx, bossy, bossFirex, bossFirey;
	// Transforms for both the enemy and his attack 
	float transform, FireTransform;
public:
	// Draw the boss to the screen
	void draw(BITMAP *buffer, BITMAP *Boss, BITMAP *Fire);
	// Follow the player
	void followPlayer ( double ninx );
	// Attack the player
	void attack ();
	// Deduct health when hit
	int deductHealth ();
	// Getters
	float Transform() { return transform; }
	double getBossX();
	double getBossY();
	double getFireX();
	double getFireY();
	// Constructer - Desctructer
	Boss();
	~Boss();
};
